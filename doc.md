# Commandes basique bash 

Pour toutes informations concernant les commandes bash et ses arguments, utilises le manuel. Exemple avec la commande grep : 

```bash
laurent@pc:~$ man grep
```
Cela t'afficheras le manuel de la commande, pour le quitter, appuies simple sur la touche `q`

## Se déplacer dans le terminal 

### CD (Change Directory)

Pour se déplacer dans un terminal, la commande `cd` fait bien le café.

```bash
laurent@pc:~$ cd Documents
```

Avec cette commande tu te retrouveras dans le dossier `Documents` (si évidemment il existe). Tu auras donc ce prompt ci-dessous : 

```bash
laurent@pc:~/Documents$
```

Le tilted que tu peux voir (**~**) indique que tu te trouve dans ton répertoire utilisateur, c'est un raccourcis. L'écriture réelle est en fait : `/home/laurent`

### PWD
Grâce à la commande `pwd` t'affiches l'endroit où tu te trouves : 

```bash
laurent@pc:~/Documents$ pwd
/home/laurent/Documents
```
### LS

La commande `ls` permet de voir le contenu d'un dossier : 

```bash
laurent@pc:~/Documents$ ls
doc.md laurent.txt max/
```
Ici, le répertoire contient deux fichiers et un répertoire. 

Deux options sont très souvent utilisées : 
- **-a** : permettant d'afficher les fichiers cachés
- **-l** : permettant de lister les droits

Les deux options peuvent être combinées pour plus de simplicité : **-la ou -al**

```bash
laurent@pc:~/Documents$ ls -la
drwxrwxr-x  2 maxence maxence 4096 févr. 29 19:24 .
drwxr-x--- 56 maxence maxence 4096 févr. 29 19:33 ..
-rwxr-x--- 56 maxence maxence 4096 févr. 29 19:33 .gitignore
-rw-rw-r--  1 maxence maxence 1406 févr. 29 19:38 doc.md
-rw-rw-r--  1 maxence maxence 1406 févr. 29 19:38 laurent.txt
drw-rw-r--  1 maxence maxence 1406 févr. 29 19:38 max
```

## Création et droits des fichiers

### MKDIR (Make Directory)
 La commande `mkdir` permet de créer un dossier, n'oublies pas d'aller voir les arguments en fonction de tes besoin : 

```bash
laurent@pc:~/Documents$ mkdir coucou
laurent@pc:~/Documents$ ls 
doc.md laurent.txt max/ coucou/
```

Le dossier `coucou` est apparu

### TOUCH

La commande `touch` permet de créer des fichiers :

```bash
laurent@pc:~/Documents$ touch salut.txt
laurent@pc:~/Documents$ ls 
doc.md salut.txt laurent.txt max/ coucou/
```

### CHMOD (Change Mod)

`chmod` est une commande permettant de changer les permissions d'un fichier et d'un dossier, il s'utilise avec un argument. Avant de comprendre comment fonctionne la commande, essayons de comprendre comment fonctionne les droits sous linux.

Prenons un ligne du résultat de la commande `ls -la`:

`-rw-rw-r--  1 maxence maxence 1406 févr. 29 19:38 laurent.txt`

Voici une image expliquant cette ligne : 

![droits](img/droits.png)